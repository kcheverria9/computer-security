package Steganography.Controllers;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;

import java.io.*;

public class VideoEmbedController {

    public Label videoName;
    public TextArea message;

    private String videoFile;
    private InputStream inputStream;
    private File file;


    public void encode() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Missing feature");
        alert.setContentText("This feature hasn't been implemented yet");
        alert.showAndWait();
    }


    public void decode() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Missing feature");
        alert.setContentText("This feature hasn't been implemented yet");
        alert.showAndWait();
    }


    public void loadVideo() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select video");
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Video Files", "*.mp4");
        fileChooser.getExtensionFilters().add(extensionFilter);

        try {
            file = fileChooser.showOpenDialog(null);
            inputStream = new FileInputStream(file);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private byte[] getVideoBytes(){
        return new byte[(int)file.length()];

    }

}
