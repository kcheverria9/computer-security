package Steganography.Controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import javax.sound.sampled.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class AudioEmbedController {

    @FXML
    Label size;
    @FXML
    private TextArea message;
    @FXML
    private Label audioName;
    private String audioFile;
    private AudioInputStream audioInputStream;
    private AudioFormat audioFormat;

    private File file;


    @FXML
    public void hideMessage() {
        if (!(audioName.getText().isEmpty())) {
            String messageToHide = message.getText();
            try {
                int bitsPerByteForEncoding = 8;
                byte[] bitsUsedForEncoding = intToByteArray(bitsPerByteForEncoding);
                byte[] messageBytes = messageToHide.getBytes();
                byte[] messageLengthBytes = intToByteArray(messageBytes.length);
                int audioBytesPosition = 0;
                byte[] audioBytes = audioStreamToBytes();

                for (int i = 0; i < 4; i++) {
                    for (int bit = 0; bit < 8; bit++) {
                        audioBytes[audioBytesPosition] = (byte) (audioBytes[audioBytesPosition] & 0xfe | (bitsUsedForEncoding[i] >>> Math.abs(7 - bit)) & 0b00000001);
                        audioBytesPosition++;
                    }
                }
                for (int i = 0; i < 4; i++) {
                    for (int bit = 0; bit < 8; bit += bitsPerByteForEncoding) {
                        audioBytes[audioBytesPosition] = (byte) ((messageLengthBytes[i] >>> Math.abs((8 - bitsPerByteForEncoding) - bit) & 0xff));
                        audioBytesPosition++;
                    }
                }
                for (byte messageByte : messageBytes) {
                    for (int bit = 0; bit < 8; bit += bitsPerByteForEncoding) {
                        audioBytes[audioBytesPosition] = (byte) ((messageByte >>> Math.abs((8 - bitsPerByteForEncoding) - bit) & 0xff));
                        audioBytesPosition++;
                    }
                }
                AudioInputStream encodedAudio = new AudioInputStream(new ByteArrayInputStream(audioBytes), audioFormat, audioInputStream.getFrameLength());
                AudioSystem.write(encodedAudio, AudioFileFormat.Type.WAVE, file);
                audioName.setText("");
                audioInputStream.close();
                message.clear();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("An error occured");
            alert.setContentText("Please select a wav audio file");
            alert.showAndWait();
        }
    }

    @FXML
    public void getMessage() {
        if (!(audioName.getText().isEmpty())) {

            try {
                int audioBytesPosition = 0;
                byte[] audioData = audioStreamToBytes();
                audioInputStream.read(audioData);

                byte[] bitsUsedForEncoding = new byte[4];

                for (int i = 0; i < 4; i++) {
                    byte currentByte = 0b0;
                    for (int bit = 0; bit < 8; bit++) {
                        byte currentBit = (byte) (audioData[audioBytesPosition] & 0x1);
                        currentByte = (byte) (currentByte << 1 | currentBit);
                        audioBytesPosition++;
                    }
                    bitsUsedForEncoding[i] = currentByte;
                }
                ByteBuffer buffer = ByteBuffer.wrap(bitsUsedForEncoding);
                buffer.order(ByteOrder.BIG_ENDIAN);
                int audioBitsForMessage = buffer.getInt();

                byte[] messageLengthBytes = new byte[4];
                for (int i = 0; i < 4; i++) {
                    byte currentByte = 0b0;
                    for (int bit = 0; bit < 8; bit += audioBitsForMessage) {
                        byte currentBit = (byte) (audioData[audioBytesPosition] & 0xFF);
                        currentByte = (byte) (currentByte << 8 | currentBit);
                        audioBytesPosition++;
                    }
                    messageLengthBytes[i] = currentByte;
                }

                buffer = ByteBuffer.wrap(messageLengthBytes);
                buffer.order(ByteOrder.BIG_ENDIAN);
                int messageLength = buffer.getInt();
                byte[] messageBytes = new byte[messageLength];

                for (int i = 0; i < messageLength; i++) {
                    byte currentByte = 0b0;
                    for (int bit = 0; bit < 8; bit += audioBitsForMessage) {
                        byte currentBit = (byte) (audioData[audioBytesPosition] & 0xFF);
                        currentByte = (byte) (currentByte << 8 | currentBit);
                        audioBytesPosition++;
                    }
                    messageBytes[i] = currentByte;
                }

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Successfully retrieved message");
                alert.setContentText(String.format("Encoded text: %s",new String(messageBytes)));
                alert.showAndWait();

                audioName.setText("");
                audioInputStream.close();
                message.clear();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please select a file to decode message in file");
            alert.showAndWait();
        }
    }


    @FXML
    public void selectAudio() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select audio");
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Audio Files","*.wav");
        fileChooser.getExtensionFilters().add(extensionFilter);
        try {
            file = fileChooser.showOpenDialog(null);
            audioFile = file.getPath();
            audioName.setText(file.getName());
            audioInputStream = AudioSystem.getAudioInputStream(file);
            audioFormat = audioInputStream.getFormat();
        } catch (UnsupportedAudioFileException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("File not supported");
            alert.showAndWait();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("File failure");
            alert.showAndWait();
        }
    }

    @FXML
    public void playSound() {
        try {
            InputStream inputStream = new FileInputStream(audioFile);
            AudioStream audioStream = new AudioStream(inputStream);
            AudioPlayer.player.start(audioStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void stopSound() {
        try {
            AudioPlayer.player.stop(audioInputStream);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private byte[] audioStreamToBytes() throws IOException {
        int frameSize = audioInputStream.getFormat().getFrameSize();
        int frameLength = (int) audioInputStream.getFrameLength();
        return new byte[frameLength * frameSize];
    }

    private byte[] intToByteArray(int value) {
        return new byte[]{
                (byte) (value >>> 24),
                (byte) (value >>> 16),
                (byte) (value >>> 8),
                (byte) (value)
        };
    }

}
