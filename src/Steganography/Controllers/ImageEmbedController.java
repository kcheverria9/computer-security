package Steganography.Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Iterator;

public class ImageEmbedController {

    @FXML
    private ImageView image;
    @FXML
    private JFXTextArea text;

    @FXML
    void onSelect(MouseEvent event) {
        JFXButton source = (JFXButton) event.getSource();

        switch (source.getId()) {
            case "embed":
                selectImage(true);
                break;
            case "extract":
                selectImage(false);
                break;
        }
    }

    private void selectImage(boolean toEmbed) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Image");
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Image Files","*.png","*.jpg");
        fileChooser.getExtensionFilters().add(extensionFilter);
        try {
            URI imageUrl = fileChooser.showOpenDialog(null).toURI();
            Image photo = new Image(imageUrl.toString());
            BufferedImage bufferedImage = ImageIO.read(imageUrl.toURL());
            image.setImage(photo);

            if (toEmbed) {
                if (text.getText().isEmpty()) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("No Text");
                    alert.setContentText("Please put text in the box in order to embed");
                    alert.showAndWait();
                    selectImage(true);
                } else {
                    byte[] toEmbedCharacters = text.getText().getBytes();
                    hideMessage(toEmbedCharacters, bufferedImage, imageUrl.getPath(), getImageFormat(imageUrl.getPath()));
                }
            } else {
                showMessage(bufferedImage);
            }
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("");
        }
    }

    private void showMessage(BufferedImage photo) {
        WritableRaster raster = photo.getRaster();
        DataBufferByte bufferByte = (DataBufferByte) raster.getDataBuffer();

        byte[] data = bufferByte.getData();

        int length = 0;
        int offset = 0;
        System.out.println("Message header is: ");
        for (int i = 0; i < 4; i += 1) {
            byte bite = 0;
            for (int j = 0; j < 8; j += 1) {
                bite |= (data[offset] & 1) << j;
                offset += 1;
            }
            System.out.print(bite);
            System.out.print(' ');
            length |= bite << (8 * i);
        }
        System.out.println();
        byte[] hiddenBytes = new byte[length];

        for (int i = 0; i < length; i += 1) {
            byte bite = 0;
            for (int j = 0; j < 8; j += 1) {
                bite |= (data[offset] & 1) << j;
                offset += 1;
            }

            hiddenBytes[i] = bite;
        }

        String output = new String(hiddenBytes);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Success");
        alert.setContentText("Embeded text: " + output);
        alert.showAndWait();
        image.setImage(null);
    }

    private void hideMessage(byte[] message, BufferedImage photo, String url, String format) {
        WritableRaster raster = photo.getRaster();
        DataBufferByte buffer = (DataBufferByte) raster.getDataBuffer();
        try {
            byte[] writableBytes = buffer.getData();
            File fileWithHiddenMessage = new File(url);
            int header = message.length;

            byte[] lenBytes = intToBytes(header);
            int totalLength = 4 + message.length;
            byte[] bytesToHide = new byte[totalLength];

            System.arraycopy(lenBytes, 0, bytesToHide, 0, lenBytes.length);
            System.arraycopy(message, 0, bytesToHide, lenBytes.length, message.length);

            if (bytesToHide.length * 8 > writableBytes.length) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Image is too small to write message");
                alert.showAndWait();
            }

            int offset = 0;
            for (byte bite : bytesToHide) {
                System.out.println(bite);
                System.out.println(' ');
                for (int j = 0; j < 8; j++) {
                    int bit = (bite >> j) & 1;
                    writableBytes[offset] = (byte) ((writableBytes[offset] & 0xfe) | bit);
                    offset++;
                }
            }
            ImageIO.write(photo, format, fileWithHiddenMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private byte[] intToBytes(int header) {
        byte[] bytes = new byte[4];
        for (int x = 0; x < 4; x++) {
            bytes[x] = (byte) ((header >> (8 * x)) & 0xff);
        }
        return bytes;
    }


    private String getImageFormat(String url) {
        try {
            ImageInputStream input = ImageIO.createImageInputStream(new FileInputStream(url));
            Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
            if (readers.hasNext()) {
                ImageReader reader = readers.next();
                reader.setInput(input);
                return reader.getFormatName();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "PNG";
    }
}
